// responsive toggle menu
$(function() {
    $('.toggler').on('click', function() {
      	$('nav').slideToggle(500);
    });
});

$('.owl-carousel').owlCarousel(
    {
        loop:true,
        margin:0,
        dots:false,
        items:1,
        nav:false,
        autoplay:true,
        smartSpeed:1000,
        autoplayHoverPause:true,
        autoplayTimeout:5000
    }
);